package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import library.entities.ILoan.LoanState;
import library.entities.IPatron.PatronState;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LoanHelper;

class TestLibrary {
	
	@Mock Library testLibrary;
	@Mock IBookHelper bookHelper;
	@Mock IPatronHelper patronHelper;
	@Mock ILoanHelper loanHelper;
	
	@Mock Patron mockPatron; 
	@Mock Loan mockLoan;
	@Mock IBook mockBook;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		testLibrary = new Library(bookHelper, patronHelper, loanHelper);
		mockPatron = mock(Patron.class);
		mockBook = mock(IBook.class);
		mockLoan = mock(Loan.class);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	
	// patronCanBorrow tests
	@Test
	void testPatronCanBorrowNoLoansNoFinesNoOverdueBooks() {
		// Arrange
		when(mockPatron.getNumberOfCurrentLoans()).thenReturn(0);
		when(mockPatron.getFinesPayable()).thenReturn(0.0);
		when(mockPatron.hasOverDueLoans()).thenReturn(false);
		boolean expected = true;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(mockPatron);
		
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void testPatronCanBorrowWithMaxLoansNoFinesNoOverdueBooks() {
		// Arrange
		when(mockPatron.getNumberOfCurrentLoans()).thenReturn(2);
		when(mockPatron.getFinesPayable()).thenReturn(0.0);
		when(mockPatron.hasOverDueLoans()).thenReturn(false);
		boolean expected = false;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(mockPatron);
		
		// Assert
		assertEquals(expected, actual);
	}
	
	@Test
	void testPatronCanBorrowWithNoLoansMaxFinesNoOverdueBooks() {
		// Arrange
		when(mockPatron.getNumberOfCurrentLoans()).thenReturn(0);
		when(mockPatron.getFinesPayable()).thenReturn(1.0);
		when(mockPatron.hasOverDueLoans()).thenReturn(false);
		boolean expected = false;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(mockPatron);
		
		// Assert
		assertEquals(expected, actual);
	}
	
	@Test
	void testPatronCanBorrowWithNoLoansNoFinesOneOverdueBook() {
		// Arrange
		when(mockPatron.getNumberOfCurrentLoans()).thenReturn(0);
		when(mockPatron.getFinesPayable()).thenReturn(0.0);
		when(mockPatron.hasOverDueLoans()).thenReturn(true);
		boolean expected = false;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(mockPatron);
		
		// Assert
		assertEquals(expected, actual);
	}
	
	
	// CommitLoan tests
	@Test
	void testCommitLoanWithVaildPendingLoan() {
		// Arrange
		doNothing().when(mockLoan).commit(any(Integer.class), any(Date.class));
		when(mockLoan.getBook()).thenReturn(mockBook);
		when(mockLoan.getPatron()).thenReturn(mockPatron);
		when(mockPatron.getNumberOfCurrentLoans()).thenReturn(0);
		when(mockPatron.getFinesPayable()).thenReturn(0.0);
		when(mockPatron.hasOverDueLoans()).thenReturn(false);
		doNothing().when(mockPatron).allowBorrowing();
		
		// Act
		testLibrary.commitLoan(mockLoan);
		
		// Assert
		List<ILoan> currentLoanList = testLibrary.getCurrentLoansList();
		List<ILoan> fullLoanList = testLibrary.getLoansList();
		// Test that the loan is on the full loan list
		assertEquals(currentLoanList.contains(mockLoan), true);
		
		// Test that the loan is on the current loan list
		assertEquals(fullLoanList.contains(mockLoan), true);
	}
	
	
	@Test
	void testCommitLoanThrowExceptionWithNonVaildPendingLoan() {
		// Arrange
		mockLoan.setLoanState(LoanState.OVER_DUE);
		doThrow(RuntimeException.class).when(mockLoan).commit(any(Integer.class), any(Date.class));
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		assertThrows(RuntimeException.class, () -> {
			testLibrary.commitLoan(mockLoan);
		});
	}
	
	
	// TODO Issue Loan
	@Test
	void testIssueLoanWhenBookIsAvailablePatronCanBorrow() {

	}
}
