package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.sql.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import library.entities.ILoan.LoanState;

class TestLoan {
	
	@Mock IBook mockBook;
	@Mock IPatron mockPatron;
	@Mock Loan testLoan;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		mockBook = mock(IBook.class);
		mockPatron = mock(IPatron.class);
		testLoan = new Loan(mockBook, mockPatron);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	
	// Commit tests
	@Test
	void testCommitLoanStatePending() {
		// Arrange
		Date testDate = new Date(0);
		assertEquals(testLoan.getLoanState(), LoanState.PENDING);
		doNothing().when(mockPatron).takeOutLoan(testLoan);
		doNothing().when(mockBook).borrowFromLibrary();
		LoanState expected = LoanState.CURRENT;
		
		// Act
		testLoan.commit(0, testDate);
		
		// Assert
		verify(mockPatron).takeOutLoan(testLoan);
		verify(mockBook).borrowFromLibrary();
		LoanState actual = testLoan.getLoanState();
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testCommitLoanThrowsExpectionWhenLoanStateNotPending() {
		// Arrange
		Date testDate = new Date(0);
		doNothing().when(mockPatron).takeOutLoan(testLoan);
		doNothing().when(mockBook).borrowFromLibrary();
		testLoan.commit(0, testDate); // Used to make loan state not pending, tested above
		assertEquals(testLoan.getLoanState(), LoanState.CURRENT);
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		assertThrows(RuntimeException.class, () -> {
			testLoan.commit(0, new Date(0));
		});
	}

	
	// checkOverDue tests
	@Test
	void testCheckOverDueLoanStateCurrentAfterLoansDueDate() {
		// Arrange
		doNothing().when(mockPatron).takeOutLoan(testLoan);
		doNothing().when(mockBook).borrowFromLibrary();
		Date testDate1 = new Date(0);
		Date testDate2 = new Date(10);
		testLoan.commit(0, testDate1); // Used to make loan state not pending, tested above
		assertEquals(testLoan.getLoanState(), LoanState.CURRENT);
		Boolean expected = true;
		LoanState expectedState = LoanState.OVER_DUE;
		
		// Act
		Boolean actual = testLoan.checkOverDue(testDate2);
		
		// Assert
		LoanState actualState = testLoan.getLoanState();
		assertEquals(expected, actual);
		assertEquals(expectedState, actualState);
	}
	
	
	@Test
	void testCheckOverDueLoanStateNotCurrentAfterLoansDueDate() {
		// Arrange
		doNothing().when(mockPatron).takeOutLoan(testLoan);
		doNothing().when(mockBook).borrowFromLibrary();
		Date testDate1 = new Date(0);
		Date testDate2 = new Date(10);
		testLoan.commit(0, testDate1); // Used to make loan state not pending, tested above
		testLoan.setLoanState(LoanState.PENDING); 
		Boolean expected = false;
		LoanState expectedState = LoanState.PENDING;
		
		// Act
		Boolean actual = testLoan.checkOverDue(testDate2);
		
		// Assert
		LoanState actualState = testLoan.getLoanState();
		assertEquals(expected, actual);
		assertEquals(expectedState, actualState);
	}
	
	
	@Test
	void testCheckOverDueLoanStateCurrentInsideLoansDueDate() {
		// Arrange
		doNothing().when(mockPatron).takeOutLoan(testLoan);
		doNothing().when(mockBook).borrowFromLibrary();
		Date testDate1 = new Date(0);
		Date testDate2 = new Date(-10);
		testLoan.commit(0, testDate1); // Used to make loan state not pending, tested above
		testLoan.setLoanState(LoanState.CURRENT); 
		Boolean expected = false;
		LoanState expectedState = LoanState.CURRENT;
		
		// Act
		Boolean actual = testLoan.checkOverDue(testDate2);
		
		// Assert
		LoanState actualState = testLoan.getLoanState();
		assertEquals(expected, actual);
		assertEquals(expectedState, actualState);
	}
}
