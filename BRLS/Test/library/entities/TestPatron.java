package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import java.sql.Date;
import java.util.List;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import library.entities.ILoan.LoanState;
import library.entities.IPatron.PatronState;


@ExtendWith(MockitoExtension.class)
class TestPatron {

	String ln = "T";
	String fn = "J";
	String em = "J@T";
	long phone = 555L;
	int id = 1;
	
	@Mock Loan mockLoan;
	@Mock ILoan mockLoan2;
	@Mock IBook mockBook;
	
	Patron testPatron;


	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		mockBook = mock(IBook.class);
		mockLoan = mock(Loan.class);
		testPatron = new Patron(ln, fn, em, phone, id);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testHasOverDueLoansNoLoans() {
		// Arrange
		boolean expected = false;
		
		// Act
		boolean actual = testPatron.hasOverDueLoans();
		
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void testHasOverDueLoansLoanInCurrentState() {
		// Arrange
		assertNotNull(mockLoan);
		testPatron.loans.put(1, mockLoan);
		when(mockLoan.isOverDue()).thenReturn(false);
		boolean expected = false;
		
		// Act
		boolean actual = testPatron.hasOverDueLoans();
		
		// Assert
		assertEquals(expected, actual);
		verify(mockLoan).isOverDue();
	}
	
	@Test
	void testHasOverDueLoansWithOverdueLoan() {
		// Arrange
		assertNotNull(mockLoan);
		testPatron.loans.put(1, mockLoan);
		when(mockLoan.isOverDue()).thenReturn(true);
		boolean expected = true;
		
		// Act
		boolean actual = testPatron.hasOverDueLoans();
		
		// Assert
		assertEquals(expected, actual);
		verify(mockLoan).isOverDue();
	}
	
	@Test
	void testHasOverDueLoansWithMultipleLoansNoneOverDue() {
		// Arrange
		testPatron.loans.put(1, mockLoan);
		testPatron.loans.put(2, mockLoan2);
		when(mockLoan.isOverDue()).thenReturn(false);
		when(mockLoan2.isOverDue()).thenReturn(false);
		boolean expected = false;
		
		// Act
		boolean actual = testPatron.hasOverDueLoans();
		
		// Assert
		assertEquals(expected, actual);
		verify(mockLoan).isOverDue();
		verify(mockLoan2).isOverDue();
	}
	
	
	// TakeOutLoan tests
	@Test
	void testTakeOutLoanCanBorrowLoanNotInExistingLoans() {
		// Arrange
		testPatron.allowBorrowing();
		assertEquals(testPatron.getCurrentState(), PatronState.CAN_BORROW);
		when(mockLoan.getId()).thenReturn(2);
		
		// Act
		testPatron.takeOutLoan(mockLoan);
		
		// Assert
		List<ILoan> loans = testPatron.getLoans();
		assertEquals(loans.contains(mockLoan), true);
	}
	
	@Test
	void testTakeOutPatronRestrictedNotInExistingLoans() {
		// Arrange
		testPatron.restrictBorrowing();
		assertEquals(testPatron.getCurrentState(), PatronState.RESTRICTED);
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		 assertThrows(RuntimeException.class, () -> {
			testPatron.takeOutLoan(mockLoan);
		});
	}
	
	
	@Test
	void testTakeOutLoanLoanAlreadyExistsInLoans() {
		// Arrange
		testPatron.allowBorrowing();
		assertEquals(testPatron.getCurrentState(), PatronState.CAN_BORROW);
		when(mockLoan.getId()).thenReturn(2);
		
		// Act
		testPatron.takeOutLoan(mockLoan);
		
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		// Confirming that the loan is already in the loans after the first time it is run
		List<ILoan> loans = testPatron.getLoans();
		assertEquals(loans.contains(mockLoan), true);
		
		 assertThrows(RuntimeException.class, () -> {
			testPatron.takeOutLoan(mockLoan);
		});
	}
}
