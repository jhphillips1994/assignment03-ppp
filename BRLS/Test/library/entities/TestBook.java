package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import library.entities.IBook.BookState;

class TestBook {
	
	Book testBook;
	String author = "A";
	String title = "T";
	String callNo = "C1";
	int bookId = 1;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		testBook = new Book(author, title, callNo, bookId);
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	
	// borrowFromLibrary tests
	@Test
	void testBorrowFromLibraryWhenBookIsAvailable() {
		// Arrange
		assertEquals(testBook.isAvailable(), true);
		boolean expected = true;
		
		// Act
		testBook.borrowFromLibrary();
		
		// Asserts
		boolean actual = testBook.isOnLoan();
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testBorrowFromLibraryWhenBookIsOnLoan() {
		// Arrange
		testBook.borrowFromLibrary();
		assertEquals(testBook.isOnLoan(), true);
		
		// Act
		Executable e = () -> testBook.borrowFromLibrary();
		
		// Assert
		assertThrows(RuntimeException.class, e);
	}

	
	// isAvilable tests
	@Test
	void testIsAvailableAfterConstruction() {
		// Arrange
		boolean expected = true;
		
		// Act
		boolean actual = testBook.isAvailable();
		
		// Asserts
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsNotAvailableWhenOnLoan() {
		// Arrange
		testBook.borrowFromLibrary();
		boolean expected = false;
		
		// Act
		boolean actual = testBook.isAvailable();
		
		// Asserts
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsNotAvailableWhenDamaged() {
		// Arrange
		testBook.borrowFromLibrary();
		testBook.returnToLibrary(true);
		boolean expected = false;
		
		// Act
		boolean actual = testBook.isAvailable();
		
		// Asserts
		assertEquals(expected, actual);
	}
}
