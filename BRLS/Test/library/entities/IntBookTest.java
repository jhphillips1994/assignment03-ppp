package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.spy;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IntBookTest {
	
	String author = "A";
	String title = "T";
	String callNo = "C1";
	int bookId = 1;
	
	Book testBook;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		testBook = new Book(author, title, callNo, bookId);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	// borrowFromLibrary tests
	@Test
	void testBorrowFromLibraryWhenBookIsAvailable() {
		// Arrange
		assertEquals(testBook.isAvailable(), true);
		boolean expected = true;
		
		// Act
		testBook.borrowFromLibrary();
		
		// Asserts
		boolean actual = testBook.isOnLoan();
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testBorrowFromLibraryWhenBookIsOnLoan() {
		// Arrange
		assertEquals(testBook.isAvailable(), true);
		
		// Act
		testBook.borrowFromLibrary();
		
		// Assert
		assertEquals(testBook.isOnLoan(), true);
		assertThrows(RuntimeException.class, () -> {
			testBook.borrowFromLibrary();
		});
	}

	
	// isAvilable tests
	@Test
	void testIsAvailableAfterConstruction() {
		// Arrange
		boolean expected = true;
		
		// Act
		boolean actual = testBook.isAvailable();
		
		// Asserts
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsNotAvailableWhenOnLoan() {
		// Arrange
		testBook.borrowFromLibrary();
		boolean expected = false;
		
		// Act
		boolean actual = testBook.isAvailable();
		
		// Asserts
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsNotAvailableWhenDamaged() {
		// Arrange
		testBook.borrowFromLibrary();
		testBook.returnToLibrary(true);
		boolean expected = false;
		
		// Act
		boolean actual = testBook.isAvailable();
		
		// Asserts
		assertEquals(expected, actual);
	}

}
