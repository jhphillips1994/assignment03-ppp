package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.spy;

import org.mockito.Mockito;
import org.mockito.Mock;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import library.entities.ILoan.LoanState;
import library.entities.IPatron.PatronState;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LoanHelper;

class IntLibraryTest {
	
	String ln = "T";
	String fn = "J";
	String em = "J@T";
	long phone = 555L;
	int id = 1;
	
	String author = "A";
	String title = "T";
	String callNo = "C1";
	int bookId = 1;
	int book2Id = 2;

	Library testLibrary;
	Loan mockLoan;
	Loan mockLoan2;
	Book mockBook;
	Book mockBook2;
	Patron mockPatron;
	LoanHelper loanHelper;

	Patron patronSpy;
	
	IBookHelper bookHelper;
	IPatronHelper patronHelper;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		loanHelper = new LoanHelper();
		testLibrary = new Library(bookHelper, patronHelper, loanHelper);
		mockPatron = new Patron(ln, fn, em, phone, id);
		mockBook = new Book(author, title, callNo, bookId);
		mockBook2 = new Book(author, title, callNo, book2Id);
		mockLoan = new Loan(mockBook, mockPatron);
		mockLoan2 = new Loan(mockBook2, mockPatron);
		
		patronSpy = spy(mockPatron);
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	// patronCanBorrow tests
	@Test
	void testPatronCanBorrowNoLoansNoFinesNoOverdueBooks() {
		// Arrange
		boolean expected = true;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(mockPatron);
		
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void testPatronCanBorrowWithMaxLoansNoFinesNoOverdueBooks() {
		// Arrange
		Date mockDate = new Date();
		mockLoan.setLoanState(LoanState.PENDING);
		mockLoan2.setLoanState(LoanState.PENDING);
		mockLoan.commit(1, mockDate);
		mockLoan2.commit(2, mockDate);
		boolean expected = false;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(patronSpy);
		
		// Assert
		assertEquals(expected, actual);
		verify(patronSpy).getNumberOfCurrentLoans();
	}
	
	@Test
	void testPatronCanBorrowWithNoLoansMaxFinesNoOverdueBooks() {
		// Arrange
		patronSpy.incurFine(2.0);
		boolean expected = false;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(patronSpy);
		
		// Assert
		assertEquals(expected, actual);
		verify(patronSpy).getFinesPayable();
	}
	
	@Test
	void testPatronCanBorrowWithNoLoansNoFinesOneOverdueBook() {
		// Arrange
		mockLoan.setLoanState(LoanState.PENDING);
		Date mockDate = new Date();
		mockLoan.commit(1, mockDate);
		mockLoan.setLoanState(LoanState.OVER_DUE);
		boolean expected = false;
		
		// Act
		boolean actual = testLibrary.patronCanBorrow(patronSpy);
		
		// Assert
		assertEquals(expected, actual);
		verify(patronSpy).hasOverDueLoans();
	}
	
	// commitLoan tests
	@Test
	void testLibraryCommitLoanWithPendingLoan() {
		// Arrange
		assertTrue(mockLoan.getLoanState() == LoanState.PENDING); // Confirm that loan is in pending state
		
		
		// Act
		testLibrary.commitLoan(mockLoan);
		
		
		// Assert
		List<ILoan> currentLoanList = testLibrary.getCurrentLoansList();
		List<ILoan> fullLoanList = testLibrary.getLoansList();
		// Test that the loan is on the full loan list
		assertEquals(currentLoanList.contains(mockLoan), true);
		
		// Test that the loan is on the current loan list
		assertEquals(fullLoanList.contains(mockLoan), true);
	}

	@Test
	void testLibraryCommitLoanThrowExceptionWithNonVaildPendingLoan() {
		// Arrange
		mockLoan.setLoanState(LoanState.OVER_DUE);
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		assertThrows(RuntimeException.class, () -> {
			testLibrary.commitLoan(mockLoan);
		});
	}

	
	// issueLoan tests
	@Test
	void testLibraryIssueLoanWhenBookIsAvailablePatronCanBorrow() {
		// Arrange
		assertTrue(mockBook.isAvailable());
		assertTrue(patronSpy.getCurrentState() == PatronState.CAN_BORROW);
		
		// Act
		ILoan result = testLibrary.issueLoan(mockBook, patronSpy);
		
		// Assert
		assertTrue(result.getLoanState() == LoanState.PENDING);
	}
	
	@Test
	void testLibraryIssueLoanWhenBookIsNotAvailablePatronCanBorrow() {
		// Arrange
		mockBook.borrowFromLibrary();
		assertTrue(mockBook.isOnLoan());
		assertTrue(patronSpy.getCurrentState() == PatronState.CAN_BORROW);
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		assertThrows(RuntimeException.class, () -> {
			ILoan result = testLibrary.issueLoan(mockBook, patronSpy);
		});
	}
	
	
	@Test
	void testLibraryIssueLoanWhenBookIsAvailablePatronCannotBorrow() {
		// Arrange
		patronSpy.restrictBorrowing();
		assertTrue(patronSpy.getCurrentState() == PatronState.RESTRICTED);
		
		// Act
		Executable e = () -> testLibrary.issueLoan(mockBook, patronSpy);
		
		// Assert
		assertThrows(RuntimeException.class, e);
	}
	
	
	@Test // TODO Find out how to create an invalid object
	void testLibraryIssueLoanWhenBookIsAvailableInvaildPatron() {
		// Arrange
		
		// Act
		Executable e = () -> testLibrary.issueLoan(mockBook, patronSpy);
		
		// Assert
		assertThrows(RuntimeException.class, e);
	}
	
	
	@Test
	void testLibraryIssueLoanWhenInvaildBookPatronCanBorrow() {
		// Arrange
		assertTrue(patronSpy.getCurrentState() == PatronState.CAN_BORROW);
		
		// Act
		Executable e = () -> testLibrary.issueLoan(mockBook, patronSpy);
		
		// Assert
		assertThrows(RuntimeException.class, e);
	}
}
