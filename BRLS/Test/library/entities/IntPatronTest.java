package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.spy;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.ILoan.LoanState;
import library.entities.IPatron.PatronState;

class IntPatronTest {
	
	String ln = "T";
	String fn = "J";
	String em = "J@T";
	long phone = 555L;
	int id = 1;
	
	String author = "A";
	String title = "T";
	String callNo = "C1";
	int bookId = 1;
	
	Patron testPatron;
	Book testBook;
	Loan testLoan;
	
	Loan loanSpy;
	Book bookSpy;
	Patron testPatronSpy;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		testPatron = new Patron(ln, fn, em, phone, id);
		testBook = new Book(author, title, callNo, bookId);
		testPatronSpy = spy(testPatron);
		bookSpy = spy(testBook);
		testLoan = new Loan(bookSpy, testPatronSpy);
		loanSpy = spy(testLoan);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testHasOverDueLoansNoLoans() {
		// Arrange
		boolean expected = false;
		
		// Act
		boolean actual = testPatronSpy.hasOverDueLoans();
		
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	void testHasOverDueLoansLoanInCurrentState() {
		// Arrange
		Date testDate = new Date(-10);
		loanSpy.commit(0, testDate);
		boolean expected = false;
		
		// Act
		boolean actual = testPatronSpy.hasOverDueLoans();
		
		// Assert
		assertEquals(expected, actual);
		verify(loanSpy).isOverDue();
	}
	
	@Test
	void testHasOVerDueLoansWithOverDueLoan() {
		// Arrange
		Date testDate = new Date(-10);
		Date testOverDueDate = new Date(0);
		loanSpy.commit(0, testDate);
		loanSpy.checkOverDue(testOverDueDate);
		boolean expected = true;
		
		// Act
		boolean actual = testPatronSpy.hasOverDueLoans();
		
		// Assert
		assertEquals(expected, actual);
		verify(loanSpy).isOverDue();
	}
	
	
	// TakeOutLoan tests
	@Test
	void testTakeOutLoanCanBorrowLoanCurrentLoanNotInExistingLoans() {
		// Arrange
		testPatronSpy.allowBorrowing();
		assertEquals(testPatronSpy.getCurrentState(), PatronState.CAN_BORROW);
		loanSpy.setLoanState(LoanState.CURRENT);
		boolean expected = true;
		
		// Act
		testPatronSpy.takeOutLoan(loanSpy);
		
		// Assert
		List<ILoan> loans = testPatronSpy.getLoans();
		assertEquals(expected, loans.contains(loanSpy));
	}
	
	@Test
	void testTakeOutPatronRestrictedCurrentLoanNotInExistingLoans() {
		// Arrange
		testPatronSpy.restrictBorrowing();
		assertEquals(testPatronSpy.getCurrentState(), PatronState.RESTRICTED);
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		 assertThrows(RuntimeException.class, () -> {
			 testPatronSpy.takeOutLoan(loanSpy);
		});
	}
	
	@Test
	void testTakeOutLoanCanBorrowLoanNotCurrentLoanNotInExistingLoans() {
		// Arrange
		testPatronSpy.allowBorrowing();
		assertEquals(testPatronSpy.getCurrentState(), PatronState.CAN_BORROW);
		assertEquals(loanSpy.getLoanState(), LoanState.PENDING);
		boolean expected = false;
		
		// Act
		testPatronSpy.takeOutLoan(loanSpy);
		
		// Assert
		List<ILoan> loans = testPatronSpy.getLoans();
		assertEquals(expected, loans.contains(loanSpy));
		// This fails due the code not checking if Loan state is current before taking out the
		// the loan
	}
	
	
	@Test
	void testTakeOutLoanLoanCurrentLoanAlreadyExistsInLoans() {
		// Arrange
		testPatronSpy.allowBorrowing();
		assertEquals(testPatronSpy.getCurrentState(), PatronState.CAN_BORROW);
		boolean expected = true;
		
		// Act
		testPatronSpy.takeOutLoan(loanSpy);
		
		
		// Assert and Act contained together to confirm the run time exception is being thrown
		// Confirming that the loan is already in the loans after the first time it is run
		List<ILoan> loans = testPatronSpy.getLoans();
		assertEquals(expected, loans.contains(loanSpy));
		
		 assertThrows(RuntimeException.class, () -> {
			 testPatronSpy.takeOutLoan(loanSpy);
		});
	}
}
