package library.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.spy;

import java.sql.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import library.entities.ILoan.LoanState;

class IntLoanTest {
	
	String ln = "T";
	String fn = "J";
	String em = "J@T";
	long phone = 555L;
	int id = 1;
	
	String author = "A";
	String title = "T";
	String callNo = "C1";
	int bookId = 1;

	Book testBook;
	Patron testPatron;
	Loan testLoan;
	
	Patron patronSpy;
	Book bookSpy;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		testPatron = new Patron(ln, fn, em, phone, id);
		testBook = new Book(author, title, callNo, bookId);
		
		patronSpy = spy(testPatron);
		bookSpy = spy(testBook);
		
		testLoan= new Loan(bookSpy, patronSpy);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	
	// commit Tests
	@Test
	void testCommitLoanStatePending() {
		// Arrange
		Date mockDate = new Date(0);
		assertTrue(testLoan.getLoanState() == LoanState.PENDING);
		
		// Act
		testLoan.commit(1, mockDate);
		
		// Assert
		verify(patronSpy).takeOutLoan(testLoan);
		verify(bookSpy).borrowFromLibrary();
		assertTrue(testLoan.getLoanState() == LoanState.CURRENT);
		List<ILoan> loans = patronSpy.getLoans();
		assertTrue(loans.contains(testLoan));
		assertTrue(bookSpy.isOnLoan());
	}
	
	
	@Test
	void testCommitLoanThrowsExpectionWhenLoanStateNotPending() {
		// Arrange
		Date mockDate = new Date(0);
		
		// Act
		testLoan.commit(1, mockDate);
		
		// Assert
		assertEquals(testLoan.getLoanState(), LoanState.CURRENT);
		assertThrows(RuntimeException.class, () -> {
			testLoan.commit(0, new Date(0));
		});
	}
	
	
	// checkOverDue tests
	@Test
	void testCheckOverDueLoanStateCurrentAfterLoansDueDate() {
		// Arrange
		Date testDate1 = new Date(0);
		Date testDate2 = new Date(10);
		testLoan.commit(0, testDate1); // Used to make loan state not pending, tested above
		assertEquals(testLoan.getLoanState(), LoanState.CURRENT);
		Boolean expected = true;
		LoanState expectedState = LoanState.OVER_DUE;
		
		// Act
		Boolean actual = testLoan.checkOverDue(testDate2);
		
		// Assert
		LoanState actualState = testLoan.getLoanState();
		assertEquals(expected, actual);
		assertEquals(expectedState, actualState);
	}
	
	
	@Test
	void testCheckOverDueLoanStateNotCurrentAfterLoansDueDate() {
		// Arrange
		Date testDate1 = new Date(0);
		Date testDate2 = new Date(10);
		testLoan.commit(0, testDate1); // Used to make loan state not pending, tested above
		testLoan.setLoanState(LoanState.PENDING); 
		Boolean expected = false;
		LoanState expectedState = LoanState.PENDING;
		
		// Act
		Boolean actual = testLoan.checkOverDue(testDate2);
		
		// Assert
		LoanState actualState = testLoan.getLoanState();
		assertEquals(expected, actual);
		assertEquals(expectedState, actualState);
	}
	
	
	@Test
	void testCheckOverDueLoanStateCurrentInsideLoansDueDate() {
		// Arrange
		Date testDate1 = new Date(0);
		Date testDate2 = new Date(-10);
		testLoan.commit(0, testDate1); // Used to make loan state not pending, tested above
		testLoan.setLoanState(LoanState.CURRENT); 
		Boolean expected = false;
		LoanState expectedState = LoanState.CURRENT;
		
		// Act
		Boolean actual = testLoan.checkOverDue(testDate2);
		
		// Assert
		LoanState actualState = testLoan.getLoanState();
		assertEquals(expected, actual);
		assertEquals(expectedState, actualState);
	}
}
