package library.borrowbook;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.borrowbook.IBorrowBookControl.BorrowControlState;
import library.entities.IBook;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.Loan;
import library.entities.helpers.BookHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class ScenarioIntBorrowBookControlTest {
	
	String ln = "T";
	String fn = "J";
	String em = "J@T";
	long phone = 555L;
	int testPatronId;
	
	String author = "A";
	String title = "T";
	String callNo = "C1";
	int testBookId;
	int testBook2Id;
	int testBook3Id;
	
	IPatron testPatron;
	IBook testBook;
	IBook testBook2;
	IBook testBook3;
	Loan testLoan;
	Loan testLoan2;
	
	IBorrowBookUI testBorrowBookUi;
	BorrowBookControl testBorrowBookController;
	Library testLibrary;
	
	LoanHelper loanHelper;
	BookHelper bookHelper;
	PatronHelper patronHelper;
	
	int testId;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		
		loanHelper = new LoanHelper();
		patronHelper = new PatronHelper();
		bookHelper = new BookHelper();
		
		testLibrary = new Library(bookHelper, patronHelper, loanHelper);
		testLibrary = spy(testLibrary);
		testPatron = testLibrary.addPatron(ln, fn, em, phone);
		testPatronId = testPatron.getId();
		testBook = testLibrary.addBook(author, title, callNo);
		testBookId = testBook.getId();
		
		testBook2 = testLibrary.addBook(author, title, callNo);
		testBook2Id = testBook.getId();
		
		testBook3 = testLibrary.addBook(author, title, callNo);
		testBook3Id = testBook.getId();
		
		testLoan = new Loan(testBook, testPatron);
		testLoan2 = new Loan(testBook, testPatron);
		
		testBorrowBookController = new BorrowBookControl(testLibrary);
		testBorrowBookUi = new BorrowBookUI(testBorrowBookController);
		testBorrowBookController = spy(testBorrowBookController);
		testBorrowBookUi = spy(testBorrowBookUi);
		testBorrowBookController.borrowBookUI = testBorrowBookUi;
		testId = 10;
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testBorrowBookNormalFlow() {
		// Arrange
		testPatron.takeOutLoan(testLoan2);
		
		// Act
		testBorrowBookController.cardSwiped(testPatronId);
		testBorrowBookController.bookScanned(testBookId);
		testBorrowBookController.commitLoans();
		
		// Assert
		verify(testBorrowBookUi).display("Completed Loan Slip");
	}
	
	
	@Test
	void testBorrowBookPatronCancelsBorrowing() {
		// Arrange
		
		// Act
		testBorrowBookController.cardSwiped(testPatronId);
		testBorrowBookController.bookScanned(testBookId);
		testBorrowBookController.cancel();
		
		// Assert
		assertTrue(testBorrowBookController.getBorrowControlState() == BorrowControlState.CANCELLED);
	}

	
	@Test
	void testBorrowBookBorrowingRestrictionsInForce() {
		// Arrange
		testPatron.incurFine(2.0);
		
		// Act
		testBorrowBookController.cardSwiped(testPatronId);
		
		// Assert
		verify(testBorrowBookUi).display("Patron cannot borrow at this time");
		assertTrue(testBorrowBookController.getBorrowControlState() == BorrowControlState.RESTRICTED);
	}
}
