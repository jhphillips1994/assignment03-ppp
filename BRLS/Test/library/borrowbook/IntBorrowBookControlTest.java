package library.borrowbook;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.borrowbook.IBorrowBookControl.BorrowControlState;
import library.entities.IBook;
import library.entities.ILoan;
import library.entities.ILoan.LoanState;
import library.entities.IPatron;
import library.entities.Library;
import library.entities.Loan;
import library.entities.helpers.BookHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class IntBorrowBookControlTest {
	
	String ln = "T";
	String fn = "J";
	String em = "J@T";
	long phone = 555L;
	int testPatronId;
	
	String author = "A";
	String title = "T";
	String callNo = "C1";
	int testBookId;
	int testBook2Id;
	int testBook3Id;
	
	IPatron testPatron;
	IBook testBook;
	IBook testBook2;
	IBook testBook3;
	Loan testLoan;
	Loan testLoan2;
	
	IBorrowBookUI testBorrowBookUi;
	BorrowBookControl testBorrowBookController;
	Library testLibrary;
	
	LoanHelper loanHelper;
	BookHelper bookHelper;
	PatronHelper patronHelper;
	
	int testId;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		loanHelper = new LoanHelper();
		patronHelper = new PatronHelper();
		bookHelper = new BookHelper();
		
		testLibrary = new Library(bookHelper, patronHelper, loanHelper);
		testLibrary = spy(testLibrary);
		testPatron = testLibrary.addPatron(ln, fn, em, phone);
		testPatronId = testPatron.getId();
		testBook = testLibrary.addBook(author, title, callNo);
		testBookId = testBook.getId();
		
		testBook2 = testLibrary.addBook(author, title, callNo);
		testBook2Id = testBook.getId();
		
		testBook3 = testLibrary.addBook(author, title, callNo);
		testBook3Id = testBook.getId();
		
		testLoan = new Loan(testBook, testPatron);
		testLoan2 = new Loan(testBook, testPatron);
		
		testBorrowBookController = new BorrowBookControl(testLibrary);
		testBorrowBookUi = new BorrowBookUI(testBorrowBookController);
		testBorrowBookController = spy(testBorrowBookController);
		testBorrowBookUi = spy(testBorrowBookUi);
		testBorrowBookController.borrowBookUI = testBorrowBookUi;
		testId = 10;
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	
	// cardSwiped tests
	@Test
	void testcardSwipedControlAndUiInSwipingStatePatronIDIsIntControlStateChanged() {
		// Arrange
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.cardSwiped(testPatronId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
		assertTrue(testBorrowBookController.borrowBookUI == testBorrowBookUi);
	}
	
	
	@Test
	void testcardSwipedControlandUiInSwipingStatePatronIDIsIntControlHasPatronReference() {
		// Arrange
		boolean expected = true;
		
		// Act
		testBorrowBookController.cardSwiped(testPatronId);
		
		// Assert
		boolean actual = testBorrowBookController.currentPatron == testPatron;
		assertEquals(expected, actual);
		verify(testLibrary).getPatronById(testPatronId);
		verify(testBorrowBookUi).setScanning();
	}
	
	
	@Test
	void testcardSwipedControlandUiInSwipingStateInvaildPatronIDStateStillSwiping() {
		// Arrange
		BorrowControlState expected = BorrowControlState.SWIPING;
		
		// Act
		testBorrowBookController.cardSwiped(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
		verify(testLibrary).getPatronById(testId);
		verify(testBorrowBookUi).display("Invalid patronId");
	}

	
	@Test
	void testcardSwipedControlandUiInSwipingStatePatronRestricted() {
		// Arrange
		testPatron.incurFine(2.0);
		BorrowControlState expected = BorrowControlState.RESTRICTED;
		
		// Act
		testBorrowBookController.cardSwiped(testPatronId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
		verify(testBorrowBookUi).setRestricted();
		verify(testBorrowBookUi).display("Patron cannot borrow at this time");
	}
	
	
	// bookScanned tests - Book Not Found
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceBookNotFoundControllStillInScanning() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(actual, expected);
		verify(testBorrowBookUi).display("Invalid bookId");
	}
	
	
	
	// bookScanned tests - Book Not Found
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceBookNotFoundNoLoanCreated() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		List<ILoan> loanList = testBorrowBookController.pendingLoans;
		boolean actual = loanList.size() == 0;
		assertEquals(actual, expected);
		verify(testBorrowBookUi).display("Invalid bookId");
	}
	
	
	// bookScanned tests - Book Not available
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceBookNotAvailableControllStillScanning() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testBook.borrowFromLibrary();
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.bookScanned(testBookId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(actual, expected);
		verify(testBorrowBookUi).display("Book cannot be borrowed");
	}	
	
	
	// bookScanned tests - Book Not available
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceBookNotAvailableNoLoanCreated() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testBook.borrowFromLibrary();
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testBookId);
		
		// Assert
		List<ILoan> loanList = testBorrowBookController.pendingLoans;
		boolean actual = loanList.size() == 0;
		assertEquals(actual, expected);
		verify(testBorrowBookUi).display("Book cannot be borrowed");
	}
	
	
	// bookScanned tests - Book available and patron would remain under loan limit
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceGetsBookIsAvailableAddsPendingLoanPatronLoanLimitNotReachedLoanInLoanList() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testBookId);
		
		// Assert
		List<ILoan> pendingLoans = testBorrowBookController.pendingLoans;
		boolean actual = pendingLoans.size() > 0;
		assertEquals(actual, expected);
	}
	
	
	// bookScanned tests - Book available and patron would remain under loan limit
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceGetsBookIsAvailableAddsPendingLoanPatronLoanLimitNotReachedControlStillScanning() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.bookScanned(testBookId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(actual, expected);
	}
	
	
	// bookScanned tests - Book available and patron would reach the loan limit
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceGetsBookIsAvailableAddsPendingLoanPatronLoanLimitReachedControllInFinalisingState() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		BorrowControlState expected = BorrowControlState.FINALISING;
		
		// Act
		testBorrowBookController.bookScanned(testBookId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(actual, expected);
		verify(testBorrowBookController).borrowingCompleted();
	}
	
	
	// bookScanned tests - Book available and patron would reach the loan limit
	@Test
	void testbookScannedControlandUiInScanningControlHasPatronReferenceGetsBookIsAvailableAddsPendingLoanPatronLoanLimitReachedLoanInLoanList() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testBookId);
		
		// Assert
		List<ILoan> pendingLoans = testBorrowBookController.pendingLoans;
		boolean actual = pendingLoans.size() == 1;
		assertEquals(actual, expected);
		verify(testBorrowBookController).borrowingCompleted();
	}
	
	
	// commitLoans tests
	@Test
	void testcommitLoansControlandUiInFinalisingReferenceToPatronPendingLoanExistsAllBooksAreAvailableControlStateCompleted() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		testBorrowBookController.bookScanned(testBookId);
		BorrowControlState expected = BorrowControlState.COMPLETED;
		
		// Act
		testBorrowBookController.commitLoans();
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
		verify(testBorrowBookUi).display("Completed Loan Slip");
		verify(testBorrowBookUi).setCompleted();
	}
	
	
	@Test
	void testcommitLoansControlandUiInFinalisingReferenceToPatronPendingLoanExistsAllBooksAreAvailableLoansAddedToLibraryCurrentLoanList() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		testBorrowBookController.bookScanned(testBookId);
		boolean expected = true;
		
		// Act
		testBorrowBookController.commitLoans();
		
		// Assert
		ILoan resultLoan = testLibrary.getCurrentLoanByBookId(testBookId);
		List<ILoan> currentLoans = testLibrary.getCurrentLoansList();
		boolean actual = currentLoans.contains(resultLoan);
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testcommitLoansControlandUiInFinalisingReferenceToPatronPendingLoanExistsAllBooksAreAvailableLoansAddedToLibraryFullLoanList() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		testBorrowBookController.bookScanned(testBookId);
		boolean expected = true;
		
		// Act
		testBorrowBookController.commitLoans();
		
		// Assert
		ILoan resultLoan = testLibrary.getCurrentLoanByBookId(testBookId);
		List<ILoan> currentLoans = testLibrary.getLoansList();
		boolean actual = currentLoans.contains(resultLoan);
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testcommitLoansControlandUiInFinalisingReferenceToPatronPendingLoanExistsAllBooksAreAvailableLoansAddedToPatronBorrowingRecord() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		testBorrowBookController.bookScanned(testBookId);
		boolean expected = true;
		
		// Act
		testBorrowBookController.commitLoans();
		
		// Assert
		ILoan resultLoan = testLibrary.getCurrentLoanByBookId(testBookId);
		List<ILoan> currentLoans = testPatron.getLoans();
		boolean actual = currentLoans.contains(resultLoan);
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testcommitLoansControlandUiInFinalisingReferenceToPatronPendingLoanExistsAllBooksAreAvailableLoanStateIsCurrent() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		testBorrowBookController.bookScanned(testBookId);
		LoanState expected = LoanState.CURRENT;
		
		// Act
		testBorrowBookController.commitLoans();
		
		// Assert
		ILoan resultLoan = testLibrary.getCurrentLoanByBookId(testBookId);
		LoanState actual = resultLoan.getLoanState();
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testcommitLoansControlandUiInFinalisingReferenceToPatronPendingLoanExistsAllBooksAreAvailableBookStateIsOnLoan() {
		// Arrange
		testBorrowBookController.cardSwiped(testPatronId);
		testPatron.takeOutLoan(testLoan2);
		testBorrowBookController.bookScanned(testBookId);
		boolean expected = true;
		
		// Act
		testBorrowBookController.commitLoans();
		
		// Assert
		IBook loanBook = testLibrary.getBookById(testBookId);
		boolean actual = loanBook.isOnLoan();
		assertEquals(expected, actual);
	}
}
