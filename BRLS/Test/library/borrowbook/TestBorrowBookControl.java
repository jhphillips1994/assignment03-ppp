package library.borrowbook;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import library.borrowbook.IBorrowBookControl.BorrowControlState;
import library.entities.Book;
import library.entities.IBook;
import library.entities.ILoan;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Patron;

class TestBorrowBookControl {
	
	@Mock IBorrowBookUI mockBorrowBookUi;
	
	@Mock BorrowBookControl testBorrowBookController;
	@Mock Library mockLibrary;
	@Mock Patron mockPatron;
	@Mock Book mockBook;
	@Mock Loan mockLoan;
	
	int testId;
	

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		mockBorrowBookUi = mock(IBorrowBookUI.class);
		mockLibrary = mock(Library.class);
		mockPatron = mock(Patron.class);
		mockBook = mock(Book.class);
		mockLoan = mock(Loan.class);
		testBorrowBookController = new BorrowBookControl(mockLibrary);
		testBorrowBookController = spy(testBorrowBookController);
		testId = 0;
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	
	// cardSwiped tests
	@Test
	void testcardSwipedControlInSwipingStatePatronIDIsIntControlStateChanged() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		when(mockLibrary.getPatronById(testId)).thenReturn(mockPatron);
		when(mockLibrary.patronCanBorrow(mockPatron)).thenReturn(true);
		doNothing().when(mockBorrowBookUi).setScanning();
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.cardSwiped(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
	}
	
	
	@Test
	void testcardSwipedControlInSwipingStatePatronIDIsIntControlHasPatronReference() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		when(mockLibrary.getPatronById(testId)).thenReturn(mockPatron);
		when(mockLibrary.patronCanBorrow(mockPatron)).thenReturn(true);
		doNothing().when(mockBorrowBookUi).setScanning();
		boolean expected = true;
		
		// Act
		testBorrowBookController.cardSwiped(testId);
		
		// Assert
		boolean actual = testBorrowBookController.currentPatron == mockPatron;
		assertEquals(expected, actual);
		verify(mockLibrary).getPatronById(testId);
		verify(mockBorrowBookUi).setScanning();
	}
	
	
	@Test
	void testcardSwipedControlInSwipingStateInvaildPatronIDStateStillSwiping() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		when(mockLibrary.getPatronById(testId)).thenReturn(null);
		doNothing().when(mockBorrowBookUi).display(any(String.class));
		BorrowControlState expected = BorrowControlState.SWIPING;
		
		// Act
		testBorrowBookController.cardSwiped(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
		verify(mockLibrary).getPatronById(testId);
		verify(mockBorrowBookUi).display("Invalid patronId");
	}

	
	@Test
	void testcardSwipedControlInSwipingStatePatronRestricted() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		when(mockLibrary.getPatronById(testId)).thenReturn(mockPatron);
		when(mockLibrary.patronCanBorrow(mockPatron)).thenReturn(false);
		doNothing().when(mockBorrowBookUi).display(any(String.class));
		doNothing().when(mockBorrowBookUi).setRestricted();
		BorrowControlState expected = BorrowControlState.RESTRICTED;
		
		// Act
		testBorrowBookController.cardSwiped(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
		verify(mockBorrowBookUi).setRestricted();
		verify(mockBorrowBookUi).display("Patron cannot borrow at this time");
	}
	
	
	// bookScanned tests - Book Not Found
	@Test
	void testbookScannedControlInScanningControlHasPatronReferenceBookNotFoundControllStillInScanning() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(null);
		doNothing().when(mockBorrowBookUi).display(any(String.class));
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(actual, expected);
		verify(mockBorrowBookUi).display("Invalid bookId");
	}
	
	// bookScanned tests - Book Not Found
	@Test
	void testbookScannedControlInScanningControlHasPatronReferenceBookNotFoundNoLoanCreated() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(null);
		doNothing().when(mockBorrowBookUi).display(any(String.class));
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		List<ILoan> loanList = testBorrowBookController.pendingLoans;
		boolean actual = loanList == null;
		assertEquals(actual, expected);
		verify(mockBorrowBookUi).display("Invalid bookId");
	}
	
	
	// bookScanned tests - Book Not available
	@Test
	void testbookScannedControlInScanningControlHasPatronReferenceBookNotAvailableControllStillScanning() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(mockBook);
		when(mockBook.isAvailable()).thenReturn(false);
		doNothing().when(mockBorrowBookUi).display(any(String.class));
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(actual, expected);
		verify(mockBorrowBookUi).display("Book cannot be borrowed");
	}	
	
	
	// bookScanned tests - Book Not available
	@Test
	void testbookScannedControlInScanningControlHasPatronReferenceBookNotAvailableNoLoanCreated() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(mockBook);
		when(mockBook.isAvailable()).thenReturn(false);
		doNothing().when(mockBorrowBookUi).display(any(String.class));
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		List<ILoan> loanList = testBorrowBookController.pendingLoans;
		boolean actual = loanList == null;
		assertEquals(actual, expected);
		verify(mockBorrowBookUi).display("Book cannot be borrowed");
	}
	
	
	// bookScanned tests - Book available and patron would remain under loan limit
	@Test
	void testbookScannedControlInScanningControlHasPatronReferenceGetsBookIsAvailableAddsPendingLoanPatronLoanLimitNotReachedLoanInLoanList() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(mockBook);
		when(mockBook.isAvailable()).thenReturn(true);
		when(mockLibrary.issueLoan(mockBook, mockPatron)).thenReturn(mockLoan);
		testBorrowBookController.pendingLoans = new ArrayList<>();
		when(mockLoan.getBook()).thenReturn(mockBook);
		doNothing().when(mockBorrowBookUi).display(mockBook);
		when(mockLibrary.patronWillReachLoanMax(mockPatron, 1)).thenReturn(false);
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		List<ILoan> pendingLoans = testBorrowBookController.pendingLoans;
		boolean actual = pendingLoans.size() > 0;
		assertEquals(actual, expected);
	}
	
	
	// bookScanned tests - Book available and patron would remain under loan limit
	@Test
	void testbookScannedControlInScanningControlHasPatronReferenceGetsBookIsAvailableAddsPendingLoanPatronLoanLimitNotReachedControlStillScanning() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(mockBook);
		when(mockBook.isAvailable()).thenReturn(true);
		when(mockLibrary.issueLoan(mockBook, mockPatron)).thenReturn(mockLoan);
		testBorrowBookController.pendingLoans = new ArrayList<>();
		when(mockLoan.getBook()).thenReturn(mockBook);
		doNothing().when(mockBorrowBookUi).display(mockBook);
		when(mockLibrary.patronWillReachLoanMax(mockPatron, 1)).thenReturn(false);
		BorrowControlState expected = BorrowControlState.SCANNING;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(actual, expected);
	}
	
	
	// bookScanned tests - Book available and patron would reach the loan limit
	@Test
	void testbookScannedControlInScanningControlHasPatronReferenceGetsBookIsAvailableAddsPendingLoanPatronLoanLimitReachedLoanInLoanList() {
		// Arrange
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(mockBook);
		when(mockBook.isAvailable()).thenReturn(true);
		when(mockLibrary.issueLoan(mockBook, mockPatron)).thenReturn(mockLoan);
		testBorrowBookController.pendingLoans = new ArrayList<>();
		when(mockLoan.getBook()).thenReturn(mockBook);
		doNothing().when(mockBorrowBookUi).display(mockBook);
		when(mockLibrary.patronWillReachLoanMax(mockPatron, 1)).thenReturn(true);
		doNothing().when(mockBorrowBookUi).display("Loan limit reached");
		doNothing().when(testBorrowBookController).borrowingCompleted();
		boolean expected = true;
		
		// Act
		testBorrowBookController.bookScanned(testId);
		
		// Assert
		List<ILoan> pendingLoans = testBorrowBookController.pendingLoans;
		boolean actual = pendingLoans.size() > 0;
		assertEquals(actual, expected);
		verify(testBorrowBookController).borrowingCompleted();
	}
	
	// commitLoans tests
	@Test
	void testcommitLoansControlInFinalisingReferenceToPatronPendingLoanExistsAllBooksAreAvailableControlStateCompleted() {
		// Arrange
		// Adding a test loan to the borrowBookControll loans list
		testBorrowBookController.setUI(mockBorrowBookUi);
		doNothing().when(mockBorrowBookUi).setSwiping();
		testBorrowBookController.setBorrowControlState(BorrowControlState.SCANNING);
		testBorrowBookController.currentPatron = mockPatron;
		when(mockLibrary.getBookById(testId)).thenReturn(mockBook);
		when(mockBook.isAvailable()).thenReturn(true);
		when(mockLibrary.issueLoan(mockBook, mockPatron)).thenReturn(mockLoan);
		testBorrowBookController.pendingLoans = new ArrayList<>();
		when(mockLoan.getBook()).thenReturn(mockBook);
		doNothing().when(mockBorrowBookUi).display(mockBook);
		when(mockLibrary.patronWillReachLoanMax(mockPatron, 1)).thenReturn(false);
		testBorrowBookController.bookScanned(testId);
		
		testBorrowBookController.setBorrowControlState(BorrowControlState.FINALISING);
		doNothing().when(mockLibrary).commitLoan(mockLoan);
		doNothing().when(mockBorrowBookUi).display("Completed Loan Slip");
		doNothing().when(mockBorrowBookUi).display(mockLoan);
		doNothing().when(mockBorrowBookUi).setCompleted();
		BorrowControlState expected = BorrowControlState.COMPLETED;
		
		// Act
		testBorrowBookController.commitLoans();
		
		// Assert
		BorrowControlState actual = testBorrowBookController.getBorrowControlState();
		assertEquals(expected, actual);
		verify(mockLibrary).commitLoan(mockLoan);
		verify(mockBorrowBookUi).display("Completed Loan Slip");
		verify(mockBorrowBookUi).display(mockLoan);
		verify(mockBorrowBookUi).setCompleted();
	}
}
