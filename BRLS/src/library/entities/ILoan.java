package library.entities;

import java.util.Date;

import library.entities.ILoan.LoanState;

public interface ILoan {

    enum LoanState {
        PENDING, CURRENT, OVER_DUE, DISCHARGED
    };

    int getId();
    
    boolean checkOverDue(Date currentDate);

    boolean isOverDue();

    Date getDueDate();
    
    LoanState getLoanState(); // TODO added for testing purposes

    IPatron getPatron();

    IBook getBook();
    
    void commit(int loanId, Date dueDate);

    void discharge(boolean isDamaged);

}